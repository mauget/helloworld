<?php
class AppController extends Controller {
	var $components=array('Auth');
	
	function beforeFilter() {
		$this->Auth->allow('*');
		$this->Auth->userModel = 'User';
		$this->Auth->fields = array('username'=> 'email','password'=>'password');
		$this->Auth->loginAction = array('admin'=> false,'controller'=>'users','action'=>'login');
		$this->Auth->loginRedirect=array('controller'=>'users','action'=>'index');
	}
	
	function isAuthorized() {
		return true;
	}
}
?>