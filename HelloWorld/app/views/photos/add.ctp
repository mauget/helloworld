<div class="photos form">
<?php echo $this->Form->create('Photo', array('enctype'=>'multipart/form-data'));?>
	<fieldset>
 		<legend><?php __('Add Photo'); ?></legend>
	<?php
		echo $this->Form->input('album_id');
		echo $this->Form->input('caption');
		echo $this->Form->input('image', array('type'=>'file'));
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit', true));?>
</div>
<div class="actions">
	<h3><?php __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('List Photos', true), array('action' => 'index'));?></li>
		<li><?php echo $this->Html->link(__('List Albums', true), array('controller' => 'albums', 'action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Album', true), array('controller' => 'albums', 'action' => 'add')); ?> </li>
	</ul>
</div>