<?php
/* Album Test cases generated on: 2011-03-20 23:45:39 : 1300661139*/
App::import('Model', 'Album');

class AlbumTestCase extends CakeTestCase {
	var $fixtures = array('app.album', 'app.user', 'app.photo');

	function startTest() {
		$this->Album =& ClassRegistry::init('Album');
	}

	function endTest() {
		unset($this->Album);
		ClassRegistry::flush();
	}

}
?>