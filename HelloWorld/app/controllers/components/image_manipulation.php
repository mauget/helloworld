<?php
class ImageManipulationComponent extends Object {
	var $_filepath;
	var $_filename;

	function __construct() {
		// Set up the location for us to save the file
		$this->_filepath = APP.'webroot'.DS.'files'.DS;
	}

	function saveImage($fileinfo){
		// Store the name of the image to our local variable
		$this->_filename= $fileinfo['name'];
		// Write the file to disk
		move_uploaded_file($fileinfo['tmp_name'], $this->_filepath.$this->_filename);
		// Call function to create thumbnail
		$this->createThumbnail();
		// Return the filename for saving in the database
		return $this->_filename;
	}

	function createThumbnail(){
		// get the file extension
		$ext = explode(".", $this->_filename);
		$ext = $ext[count($ext)-1];
		// Store the full file name
		$filename = $this->_filepath . $this->_filename;
		$t_filename = $this->_filepath . 'thumb_'. $this->_filename;
		// Set max thumbnail width and heigh
		$xmax = 150;
		$ymax = 150;
		$img = null;

		$lcExt = strtolower(ext);
		if($lcExt=="jpg" || $lcExt=="jpeg")	$img = imagecreatefromjpeg($filename);
		elseif ($lcExt == "png") $img = imagecreatefrompng($filename);
		elseif($lcExt=="gif")	$img = imagecreatefromgif($filename);
		else return; // not supported file type

		$x = imagesx($img);
		$y = imagesy($img);
		// if image is already smaller than thumbnail, don't resize, just save
		if($x <= $xmax && $y <= $ymax) {
			copy($filename, $t_filename);
			imagedestroy($img);
			return;
		} elseif($x >= $y){
			$newx = $xmax;
			$newy = $newx*$y/$x;
		} else {
			// Otherwise resize the width to make it proportional
			$newy = $ymax;
			$newx=$x/$y*$newy;
		}
		$img2 = imagecreatetruecolor ($newx, $newy);
		imagecopyresized($img2, $img, 0,0,0,0,floor($newx),floor($newy),$x,$y);

		// Save the resized image
		if ($lcExt=="jpg"||$lcExt="jpeg") imagejpeg($img2, $t_filename);
		elseif($lcExt="png") imagepng($img2, $t_filename);
		elseif($lcExt="gif") imagegif($img2, $t_filename);

		imagedestroy($img2);
		imagedestroy($img);
	}
}

?>