<?php
class PhotosController extends AppController {

	var $name = 'Photos';
	var $components = array ('Session', 'Auth', 'ImageManipulation');

	function beforeFilter() {
		parent::beforeFilter();
		if (!$this->Auth->user()) {
			$this->Auth->deny('add', 'edit', 'delete');
		}
	}

	function index() {
		$this->Photo->recursive = 0;
		$this->set('photos', $this->paginate());
	}

	function view($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid photo', true));
			$this->redirect(array('action' => 'index'));
		}
		$this->set('photo', $this->Photo->read(null, $id));
	}

	function add() {
		if (!empty($this->data)) {
			// Fully-qualified image file name : $this->data['Photo']['image']
			$fileInfo = $this->data['Photo']['image'];

//			$this->Session->setFlash(__('Debug: '. $fileInfo['name'], true));

			// Call our component, return transformed file name
			$this->data['Photo']['image'] = $this->ImageManipulation->saveImage($fileInfo);

			$this->Photo->create();
			if ($this->Photo->save($this->data)) {
				$this->Session->setFlash(__('The photo has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The photo could not be saved. Please, try again.', true));
			}
		}
		$albums = $this->Photo->Album->find('list');
		$this->set(compact('albums'));
	}

	function edit($id = null) {
		if (!$id && empty($this->data)) {
			$this->Session->setFlash(__('Invalid photo', true));
			$this->redirect(array('action' => 'index'));
		}
		if (!empty($this->data)) {
			if ($this->Photo->save($this->data)) {
				$this->Session->setFlash(__('The photo has been saved', true));
				$this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('The photo could not be saved. Please, try again.', true));
			}
		}
		if (empty($this->data)) {
			$this->data = $this->Photo->read(null, $id);
		}
		$albums = $this->Photo->Album->find('list');
		$this->set(compact('albums'));
	}

	function delete($id = null) {
		if (!$id) {
			$this->Session->setFlash(__('Invalid id for photo', true));
			$this->redirect(array('action'=>'index'));
		}
		if ($this->Photo->delete($id)) {
			$this->Session->setFlash(__('Photo deleted', true));
			$this->redirect(array('action'=>'index'));
		}
		$this->Session->setFlash(__('Photo was not deleted', true));
		$this->redirect(array('action' => 'index'));
	}

}
?>